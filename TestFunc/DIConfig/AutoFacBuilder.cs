﻿using Autofac;
using AzureFunctions.Autofac.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFunc.Interface;

namespace TestFunc.DIConfig
{
   public class AutoFacBuilder
    {
        public AutoFacBuilder(string functionName)
        {
            DependencyInjection.Initialize(builder =>
            {
                builder.RegisterType<DateTimeHelper>().As<IDateTimeHelper>(); // Naive

            }, functionName);
        }
    }
}
