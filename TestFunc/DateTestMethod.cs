﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFunc.Interface;

namespace TestFunc
{
   public class DateTestMethod
    {
        private IDateTimeHelper _dateTimeHelper;
        public DateTestMethod(IDateTimeHelper dateTimeHelper)
        {
            _dateTimeHelper = dateTimeHelper;
        }

        public bool IsThereWorldCupThisYear()
        {
            var currentYear = _dateTimeHelper.GetDateTimeNow().Year;
            return ((currentYear - 1998) % 4) == 0;
        }
    }
}
