using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AzureFunctions.Autofac;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using TestFunc.DIConfig;
using TestFunc.Interface;

namespace TestFunc
{

    [DependencyInjectionConfig(typeof(AutoFacBuilder))]
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log, [Inject] IDateTimeHelper dateTime)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            if (name == null)
            {
                // Get request body
                dynamic data = await req.Content.ReadAsAsync<object>();
                name = data?.name;
            }


            //get keyvault secret

            String secretValue = await Helper.GetSecretValue("applicationSecret2");
            log.Info($"The value of secret value is {secretValue}");
            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, $"Please pass a name on the query string or in the request body {dateTime.GetDateTimeNow()}")
                : req.CreateResponse(HttpStatusCode.OK, $"Hello {name} {dateTime.GetDateTimeNow()} {secretValue} using keyvault Syntax from app settings {Environment.GetEnvironmentVariable("secret2")}");
        }
    }

    public static class Helper { 

    public static async Task<string> GetSecretValue(string keyName)
    {
        string secret = "";

        AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();
        var keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));
            Console.WriteLine(Environment.GetEnvironmentVariable("keyvault") + keyName);
            //slow without ConfigureAwait(false)
            var secretBundle = await keyVaultClient.GetSecretAsync(Environment.GetEnvironmentVariable("keyvault") + keyName).ConfigureAwait(false);

        secret = secretBundle.Value;
            Console.WriteLine(secret);
        return secret;
    }
    }

}
