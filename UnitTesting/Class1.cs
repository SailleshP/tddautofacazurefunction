﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFunc;
using TestFunc.Interface;

namespace UnitTesting
{

    [TestFixture]
    public class TimerMock
    {


        [Test]
        public void TestDateTime()
        {
            var mockDateTimeHelper = new Mock<IDateTimeHelper>();
            var fakeDate = new DateTime(2018, 05, 15);
            mockDateTimeHelper.Setup(o => o.GetDateTimeNow()).Returns(fakeDate);
            var worldCupHandler = new DateTestMethod(mockDateTimeHelper.Object);
             var result = worldCupHandler.IsThereWorldCupThisYear();
            Assert.That(result, Is.EqualTo(true));

        }

        [Test]
        public void IsThereWorldCupThisYear_WhenNonWorldCupYear_ReturnsFalse()
        {
            var mockDateTimeHelper = new Mock<IDateTimeHelper>();
            var fakeDate = new DateTime(2020, 07, 10);
            mockDateTimeHelper.Setup(o => o.GetDateTimeNow()).Returns(fakeDate);
            var worldCupHandler = new DateTestMethod(mockDateTimeHelper.Object);
            var result = worldCupHandler.IsThereWorldCupThisYear();
            Assert.That(result, Is.EqualTo(false));
        }

    }
}
